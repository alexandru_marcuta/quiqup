**Requirements:**

I would like him to create an automated test of a simple order on our web app, and mobile customer iOS and Android apps, and also demonstrate an automated test of our open API endpoints. Is this something he would be able to do without needing access to our source code.

**Details** 

_API enpoints:_ 
http://api-docs.quiqup.com/v0.0.1/welcome/ZLszq8qQSokhFfqD3

_UI endpoint:_
https://web-staging.quiqup.com/

_Card details:_
4242 4242 4242 4242 exp 10/17 cvv 123 postcode nw106rb for a positive test and 
4000 0000 0000 0119 with the same exp, cvv and pcode for a negative test

**Findings**

UI automation tests: In order to easier automate the tests, we will need to talk to developers to setup and id or test-id for every HTML element. It was difficult in the beginning to select the elements with the selenium web drivers. Also, in order to run the UI tests, you will need to have Firefox installed on your computer. (I use on mine Firefox v35.0)

API automation tests: I have created some tests for 3 of the APIs (Authentication, Postcodes and Addresses). For APIs such as Addresses or PostJob It is kind of difficult to map all the information in the response, because there is a big JSON file that is returned. I believe that all the information is necessary, so I guess this cannot be made smaller.