package uitesting.util;

import javax.naming.ConfigurationException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Created by amarcuta on 1/16/17.
 */
public class PropertiesUtils {

    public static final String ENVIRONMENT_CONFIG_FILE = "environment.config.file";
    private static final String PROPERTIES_PATH = "target/classes/environments/localhost/environment.properties";
    private static Properties properties = new Properties();

    public static void setProperties() throws ConfigurationException, IOException {
        String propertiesPath = System.getProperty(ENVIRONMENT_CONFIG_FILE);
        if (propertiesPath == null || propertiesPath.isEmpty()) {
            propertiesPath = PROPERTIES_PATH;
        }

        String filePath = Paths.get(propertiesPath).toAbsolutePath().toString();

        InputStream input = new FileInputStream(filePath);
        properties.load(input);
        input.close();
    }

    public static String getProperty(String name) {
        return properties.getProperty(name);
    }
}
