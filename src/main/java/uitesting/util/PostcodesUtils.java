package uitesting.util;

/**
 * Created by amarcuta on 24/01/2017.
 */
public class PostcodesUtils {

    public enum Postcode {
        VALID_ADDRESS("W2 2SG"),
        VALID_CARD("NW106RB");

        private final String text;

        /**
         * @param text
         */
        private Postcode(final String text) {
            this.text = text;
        }

        /* (non-Javadoc)
         * @see java.lang.Enum#toString()
         */
        @Override
        public String toString() {
            return text;
        }
    }
}
