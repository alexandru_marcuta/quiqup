package uitesting.util;

/**
 * Created by amarcuta on 25/01/2017.
 */
public class StringUtils {

    public static String getFirstWord(String string) {
        String firstWord = null;
        if(string.contains("-")){
            firstWord= string.substring(0, string.indexOf("-"));
        }

        return firstWord;
    }
}
