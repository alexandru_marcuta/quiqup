package uitesting.util;

/**
 * Created by amarcuta on 1/17/17.
 */
public class TimeoutUtils {

    public static void waitFor(Integer miliSeconds) {
        try {
            Thread.sleep(miliSeconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
