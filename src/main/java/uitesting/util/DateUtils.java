package uitesting.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by amarcuta on 25/01/2017.
 */
public class DateUtils {

    public static String getTodayDate () {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        String todayDate = dateFormat.format(date);

        return todayDate;
    }
}
