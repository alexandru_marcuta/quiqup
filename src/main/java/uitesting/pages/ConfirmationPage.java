package uitesting.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.*;

import uitesting.util.TimeoutUtils;

/**
 * Created by amarcuta on 25/01/2017.
 */
public class ConfirmationPage extends PageObject {

    public Boolean getConfirmationMessage(String confirmationMessage) {

        TimeoutUtils.waitFor(2000);
        WebElementFacade confirmationMessageContainer = find(By.xpath("//h1[text()=\"" + confirmationMessage + "\"]"));;

        return confirmationMessageContainer.isDisplayed();
    }

    public String getOrderNumber() {

        WebElementFacade orderNumberContainer = find(By.xpath("//h1[@class='bar-title ng-binding']"));;

        return orderNumberContainer.getText();
    }
}
