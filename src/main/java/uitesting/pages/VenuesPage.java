package uitesting.pages;

import net.serenitybdd.core.pages.*;
import net.serenitybdd.core.annotations.findby.By;

import uitesting.util.TimeoutUtils;

/**
 * Created by amarcuta on 24/01/2017.
 */
public class VenuesPage extends PageObject {

    public String checkPostcode() {

        TimeoutUtils.waitFor(2000);
        String postcodePlaceholderValue = find(By.xpath("//input[@placeholder='Postcode']")).getValue();
        return postcodePlaceholderValue;
    }

    public void selectVenue(String venueName) {

        TimeoutUtils.waitFor(2000);
        WebElementFacade theQuiqBarby31DoverLink = find(By.xpath("//strong[text()=\"" + venueName + "\"]"));
        theQuiqBarby31DoverLink.click();
    }

}
