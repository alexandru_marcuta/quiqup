package uitesting.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.*;

import uitesting.util.TimeoutUtils;

/**
 * Created by amarcuta on 24/01/2017.
 */
public class CheckoutPage extends PageObject {

    private String newAddress = "New address";
    private String save = "Save";
    private String addNewCard = "Add new card";
    private String getItNow = "Get it now";
    private String verify = "Verify";

    //Address details
    public void clickNewAddress() {

        TimeoutUtils.waitFor(2000);
        WebElementFacade newAddressLink = find(By.xpath("//a[text()=\"" + newAddress + "\"]"));
        newAddressLink.click();
    }

    public void addPostcode(String postcode) {

        TimeoutUtils.waitFor(2000);
        WebElementFacade postcodeInputField = find(By.xpath("//input[@name='postcode']"));
        postcodeInputField.clear();
        postcodeInputField.sendKeys(postcode);
    }

    public void clickCheckPostcode() {

        WebElementFacade searchPostcodeButton = find(By.xpath("//input[@name='postcode']/../button"));
        searchPostcodeButton.click();
    }

    public void selectAddress(String address) {

        TimeoutUtils.waitFor(2000);
        WebElementFacade selectAddressLink = find(By.xpath("//li[text()=\"" + address + "\"]"));
        selectAddressLink.click();
    }

    public void saveAddress() {

        TimeoutUtils.waitFor(2000);
        WebElementFacade saveAddressButton = find(By.xpath("//button[text()=\"" + save + "\"]"));
        saveAddressButton.click();
    }


    //Card details
    public void clickAddNewCard() {

        TimeoutUtils.waitFor(2000);
        WebElementFacade newAddressLink = find(By.xpath("//a[text()=\"" + addNewCard + "\"]"));
        newAddressLink.click();
    }

    public void addCardNumber(String cardNumber) {

        TimeoutUtils.waitFor(2000);
        WebElementFacade cardNumberInputField = find(By.xpath("//input[@name='card_number']"));
        cardNumberInputField.clear();
        cardNumberInputField.sendKeys(cardNumber);
    }

    public void addExpiryMonth(Integer expiryMonth) {

        WebElementFacade expiryMonthInputField = find(By.xpath("//input[@name='expire_month']"));
        expiryMonthInputField.clear();
        expiryMonthInputField.sendKeys(expiryMonth.toString());
    }

    public void addExpiryYear(Integer expiryYear) {

        WebElementFacade expiryYearInputField = find(By.xpath("//input[@name='expire_year']"));
        expiryYearInputField.clear();
        expiryYearInputField.sendKeys(expiryYear.toString());
    }

    public void addCcv(Integer ccv) {

        WebElementFacade ccvInputField = find(By.xpath("//input[@name='cvc']"));
        ccvInputField.clear();
        ccvInputField.sendKeys(ccv.toString());
    }

    public void saveCard() {

        TimeoutUtils.waitFor(2000);
        WebElementFacade saveAddressButton = find(By.xpath("//button[text()=\"" + save + "\"]"));
        saveAddressButton.click();
    }

    public void clickGetProductNow() {

        TimeoutUtils.waitFor(2000);
        WebElementFacade getItNowButton = find(By.xpath("//button[text()=\"" + getItNow + "\"]"));
        getItNowButton.click();
    }

    public void addCode(String code) {

        TimeoutUtils.waitFor(2000);
        WebElementFacade verificationCodeInputField = find(By.xpath("//input[@type='tel']"));
        verificationCodeInputField.clear();
        verificationCodeInputField.sendKeys(code);
    }

    public void verifyCode() {

        TimeoutUtils.waitFor(2000);
        WebElementFacade verifyCodeButton = find(By.xpath("//button[text()=\"" + verify + "\"]"));
        verifyCodeButton.click();
    }
}
