package uitesting.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.*;

import java.util.ArrayList;

import uitesting.util.TimeoutUtils;

/**
 * Created by amarcuta on 24/01/2017.
 */
public class ProductsPage extends PageObject {

    private String getItNow = "GET IT NOW";

    public ArrayList<Boolean> checkVenue(String venue) {

        TimeoutUtils.waitFor(6000);
        WebElementFacade theVenueName = find(By.xpath("//h1[text()=\"" + venue + "\"]"));

        ArrayList<Boolean> list =  new ArrayList<Boolean>();
        list.add(theVenueName.isDisplayed());
        list.add(theVenueName.isVisible());
        list.add(theVenueName.isEnabled());

        return list;
    }

    public void selectProduct(String productName) {

        TimeoutUtils.waitFor(2000);
        WebElementFacade theGiftBoxwithStylishRibbonLink = find(By.xpath("//div[text()=\"" + productName + "\"]"));
        theGiftBoxwithStylishRibbonLink.click();
    }

    public Boolean checkProduct(String product) {

        TimeoutUtils.waitFor(5000);
        WebElementFacade theProductName = find(By.xpath("//strong[text()=\"" + product + "\"]"));

        return theProductName.isDisplayed();
    }

    public void clickGetProductNow() {

        WebElementFacade getItNowButton = find(By.xpath("//button[text()=\"" + getItNow + "\"]"));
        getItNowButton.click();
    }
}
