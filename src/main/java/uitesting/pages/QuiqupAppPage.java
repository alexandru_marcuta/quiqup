package uitesting.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.*;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import uitesting.util.TimeoutUtils;

/**
 * Created by amarcuta on 1/17/17.
 */
public class QuiqupAppPage extends PageObject {

    WebDriver driver;
    JavascriptExecutor js = (JavascriptExecutor) driver;

    //Login-logout functions
    private final String login = "Log in";
    private final String logout = "Logout";

    @FindBy(xpath = "//button[text()=\"" + login + "\"]")
    private WebElementFacade loginButton;

    @FindBy(name = "email")
    private WebElementFacade usernameField;

    @FindBy(name = "password")
    private WebElementFacade passwordField;

    private WebElementFacade logOut;

    protected WebElementFacade getLoginButtonLabel() { return loginButton; }

    public void clickLogin() { getLoginButtonLabel().click(); }

    protected WebElementFacade getUsername() { return usernameField; }

    public void setUsername(String username) { getUsername().sendKeys(username); }

    protected WebElementFacade getPassword() {
        return passwordField;
    }

    public void setPassword(String password) {
        getPassword().sendKeys(password);
    }

    public void clickSignIn() {

        WebElementFacade signInButton = find(By.xpath("//input[@name='email']/../../../section[1]/button"));
        signInButton.click();
    }

    public WebElementFacade signOut(WebDriver webdriver) {

        //WebElementFacade logo = find(By.xpath("//div[text()=\"" + quiqup + "\"]"));
        TimeoutUtils.waitFor(2000);
        WebElementFacade logo = find(By.tagName("drop-down-menu"));

        Actions actions = new Actions(webdriver);
        Actions moveOverLogo = actions.moveToElement(logo);
        moveOverLogo.build().perform();

        TimeoutUtils.waitFor(2000);
        WebElementFacade logOut = find(By.xpath("//a[text()=\"" + logout + "\"]"));
        logOut.click();

        return logOut;
    }

    //Addresses functions
    public void addPostcode(String postcode) {

        WebElementFacade postcodeInputField = find(By.className("postcode-input"));
        postcodeInputField.sendKeys(postcode);
    }

    public void clickCheckPostcode() {

        WebElementFacade searchPostcodeButton = find(By.xpath("//input[@class='postcode-input']/../button"));
        searchPostcodeButton.click();
    }
}
