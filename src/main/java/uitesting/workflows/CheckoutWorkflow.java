package uitesting.workflows;

import net.thucydides.core.annotations.*;
import net.thucydides.core.steps.ScenarioSteps;

import uitesting.steps.CheckoutSteps;

/**
 * Created by amarcuta on 24/01/2017.
 */
public class CheckoutWorkflow extends ScenarioSteps {

    @Steps
    public CheckoutSteps steps;

    @StepGroup
    public void addNewAddress(String postcode, String address) {
        steps.clickNewAddress();
        steps.addPostcode(postcode);
        steps.addAddress(address);
    }

    @StepGroup
    public void addNewCard(String cardNumber, Integer expiryMonth, Integer expiryYear,
                           Integer ccv, String postcode) {
        steps.clickAddNewCard();
        steps.addCardDetails(cardNumber, expiryMonth, expiryYear, ccv, postcode);
    }

    @StepGroup
    public void getProductNow(String code) {
        steps.clickGetProductNow();
        steps.mobileVerification(code);
    }

    @StepGroup
    public Boolean checkConfirmationMessage(String message) {
        return steps.checkConfirmationMessage(message);
    }

    @StepGroup
    public String getConfirmationOrder()  {
        return steps.getConfirmatioOrder();
    }
}
