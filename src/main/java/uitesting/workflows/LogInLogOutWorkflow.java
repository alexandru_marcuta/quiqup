package uitesting.workflows;

import net.thucydides.core.annotations.StepGroup;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.ScenarioSteps;
import org.openqa.selenium.WebDriver;
import uitesting.steps.LoginSteps;

/**
 * Created by amarcuta on 1/17/17.
 */
public class LogInLogOutWorkflow extends ScenarioSteps {

    @Steps
    public LoginSteps steps;

    @StepGroup
    public void logIn(String username, String password) {
        steps.open();
        steps.logIn(username, password);
    }

    @StepGroup
    public void logOut(WebDriver webdriver) {
        steps.logOut(webdriver);
    }
}
