package uitesting.workflows;

import net.thucydides.core.annotations.*;
import net.thucydides.core.steps.ScenarioSteps;

import uitesting.steps.AddressSteps;

/**
 * Created by amarcuta on 24/01/2017.
 */
public class AddressWorkflow extends ScenarioSteps {

    @Steps
    public AddressSteps steps;

    @StepGroup
    public String addPostcode(String postcode) {
        return steps.addPostcode(postcode);
    }
}
