package uitesting.workflows;

import net.thucydides.core.annotations.*;
import net.thucydides.core.steps.ScenarioSteps;

import java.util.ArrayList;

import uitesting.steps.*;

/**
 * Created by amarcuta on 24/01/2017.
 */
public class VenuesWorkflow extends ScenarioSteps {

    @Steps
    public VenuesSteps steps;

    @StepGroup
    public void selectVenue(String venueName) {
        steps.selectVenue(venueName);
    }

    @StepGroup
    public ArrayList<Boolean> checkVenue(String venue)  {
        return steps.checkVenue(venue);
    }
}
