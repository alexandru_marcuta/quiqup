package uitesting.workflows;

import net.thucydides.core.annotations.*;
import net.thucydides.core.steps.ScenarioSteps;

import uitesting.steps.ProductsSteps;

/**
 * Created by amarcuta on 24/01/2017.
 */
public class ProductsWorkflow extends ScenarioSteps {

    @Steps
    public ProductsSteps steps;

    @StepGroup
    public void selectProduct(String productName) {
        steps.selectProduct(productName);
    }

    @StepGroup
    public Boolean checkProduct(String product)  {
        return steps.checkProduct(product);
    }

    @StepGroup
    public void getProductNow() {
        steps.clickGetProductNow();
    }
}
