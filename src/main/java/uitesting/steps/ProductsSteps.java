package uitesting.steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import uitesting.pages.ProductsPage;

/**
 * Created by amarcuta on 24/01/2017.
 */
public class ProductsSteps extends ScenarioSteps {

    ProductsPage productsPage;

    @Step
    public void selectProduct(String productName) {
        productsPage.selectProduct(productName);
    }

    @Step
    public Boolean checkProduct(String product)  {
        return productsPage.checkProduct(product);
    }

    @Step
    public void clickGetProductNow() {
        productsPage.clickGetProductNow();
    }
}
