package uitesting.steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import uitesting.pages.*;

/**
 * Created by amarcuta on 24/01/2017.
 */
public class AddressSteps extends ScenarioSteps {

    QuiqupAppPage quiqupAppPage;
    VenuesPage venuesPage;

    @Step
    public String addPostcode(String postcode) {
        quiqupAppPage.addPostcode(postcode);
        quiqupAppPage.clickCheckPostcode();
        return venuesPage.checkPostcode();
    }
}
