package uitesting.steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import java.util.ArrayList;

import uitesting.pages.*;

/**
 * Created by amarcuta on 24/01/2017.
 */
public class VenuesSteps extends ScenarioSteps {

    VenuesPage venuesPage;
    ProductsPage productsPage;

    @Step
    public void selectVenue(String venueName) {
        venuesPage.selectVenue(venueName);
    }

    @Step
    public ArrayList<Boolean> checkVenue(String venue)  {
        return productsPage.checkVenue(venue);
    }
}
