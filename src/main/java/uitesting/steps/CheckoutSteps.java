package uitesting.steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import uitesting.pages.*;

/**
 * Created by amarcuta on 24/01/2017.
 */
public class CheckoutSteps extends ScenarioSteps {

    CheckoutPage checkoutPage;
    ConfirmationPage confirmationPage;

    @Step
    public void clickNewAddress() {
        checkoutPage.clickNewAddress();
    }

    @Step
    public void addAddress(String address) {
        checkoutPage.selectAddress(address);
        checkoutPage.saveAddress();
    }

    @Step
    public void addPostcode(String postcode) {
        checkoutPage.addPostcode(postcode);
        checkoutPage.clickCheckPostcode();
    }

    @Step
    public void clickAddNewCard() {
        checkoutPage.clickAddNewCard();
    }

    @Step
    public void addCardDetails(String cardNumber, Integer expiryMonth, Integer expiryYear,
                               Integer ccv, String postcode) {
        checkoutPage.addCardNumber(cardNumber);
        checkoutPage.addExpiryMonth(expiryMonth);
        checkoutPage.addExpiryYear(expiryYear);
        checkoutPage.addCcv(ccv);
        checkoutPage.addPostcode(postcode);
        checkoutPage.saveCard();
    }

    @Step
    public void clickGetProductNow() {
        checkoutPage.clickGetProductNow();
    }

    @Step
    public void mobileVerification(String code) {
        checkoutPage.addCode(code);
        checkoutPage.verifyCode();
    }

    @Step
    public Boolean checkConfirmationMessage(String message)  {
        return confirmationPage.getConfirmationMessage(message);
    }

    @Step
    public String getConfirmatioOrder()  {
        return confirmationPage.getOrderNumber();
    }
}
