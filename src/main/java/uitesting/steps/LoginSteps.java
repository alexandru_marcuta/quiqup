package uitesting.steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uitesting.pages.QuiqupAppPage;

;

/**
 * Created by amarcuta on 1/17/17.
 */
public class LoginSteps extends ScenarioSteps {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginSteps.class);

    private Pages pages;
    QuiqupAppPage quiqupAppPage;

    public LoginSteps(Pages pages) {
        super(pages);
        this.pages=pages;
    }

    @Step
    public void logIn(String username, String password) {
        quiqupAppPage.clickLogin();
        quiqupAppPage.setUsername(username);
        quiqupAppPage.setPassword(password);
        quiqupAppPage.clickSignIn();
    }

    @Step
    public void logOut(WebDriver webdriver) {
        quiqupAppPage.signOut(webdriver);
    }

    public void open() {
        LOGGER.info(">> Open Page: {}", this.pages.getDefaultBaseUrl());
        this.quiqupAppPage.open();
    }
}
