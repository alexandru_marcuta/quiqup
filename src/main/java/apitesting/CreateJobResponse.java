package apitesting;

/**
 * Created by amarcuta on 1/19/17.
 */
public class CreateJobResponse {

    private String transport_mode;
    private String id;
    private String billing_identifier;
    private String state;
    private String kind;
    private class estimated_costs {
        private String fixed_cost;
        private String waypoint_cost;
        private String kilometer_cost;
        private String period_cost;
        private String adjustment_cost;
        private String vat_cost;
        private String total_vat_exclusive_cost;
        private String total_vat_inclusive_cost;
        private class errors {
            private String base;
        }
    };
    private class known_costs {
        private String total_cost;
        private String delivery_cost;
        private String pickups_cost;
        private String adjustment_cost;
        private String subtotal_cost;
        private String service_fee;
        private String service_fee_percent;
        private String surcharge_cost;
        private class errors {
            private String base;
        }
    };
    private String courier_id;
    private String courier_name;
    private String scheduled_for;
    private String earliest_collection_at;
    private class metadata{
        private String id;
    };
    private String courier;
    private String created_at;
    private String submitted_at;
    private String delivery_window_minutes;
    private class orders {
        private class pickup {
            private String contact_name;
            private String contact_phone;
            private class location {
                private String address1;
                private String address2;
                private String town;
                private String county;
                private String postcode;
                private String partner_location_id;
                private Integer[] coords;
                private String notes;
            }
        };
        private class dropoff {
            private String contact_name;
            private String contact_phone;
            private class location {
                private String address1;
                private String address2;
                private String town;
                private String county;
                private String postcode;
                private String partner_location_id;
                private Integer[] coords;
                private String notes;
            }
        };
        private class items {
            private Integer id;
            private String name;
            private Integer quantity;
            private Integer price;
            private String notes;
            private String source_gid;
            private class children {
                private String source_gid;
                private Integer quantity;
            }
        }
    };
    private class courier {
        private String transport_mode;
    }
}
