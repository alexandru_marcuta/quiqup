package apitesting;

import apitesting.util.PropertiesUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Created by amarcuta on 19/01/2017.
 */
public class Postcode {

    static Logger logger = LoggerFactory.getLogger(Authentication.class);

    static RestTemplate restTemplate = new RestTemplate();

    public static ResponseEntity<PostcodeResponse> checkPostcode(String postcode) {

        String url = PropertiesUtils.STAGE.concat("/active_locations");

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json;charset=UTF-8");

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("postcode", postcode);

        HttpEntity<?> entity = new HttpEntity(headers);


        ResponseEntity<PostcodeResponse> response = restTemplate.exchange(builder.build().encode().toUri(),
                HttpMethod.GET, entity, PostcodeResponse.class);

        PostcodeResponse postcodeResponse = response.getBody();

        return response;
    }

    private static ResponseEntity<String> takePostcodeResponse(HttpHeaders headers, UriComponentsBuilder builder){

        HttpEntity<?> entity = new HttpEntity(headers);
        ResponseEntity<String> response = restTemplate.exchange(builder.build().encode().toUri(),
                HttpMethod.GET, entity, String.class);

        if (logger.isInfoEnabled()) {
            logger.info(response.toString());
        }

        return response;
    }
}
