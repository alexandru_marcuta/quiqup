package apitesting;

/**
 * Created by amarcuta on 1/19/17.
 */
public class PostcodeResponse {

    private Boolean location_supported;
    private String earliest_collection_at;

    public Boolean getLocation_supported() {
        return location_supported;
    }

    public void setLocation_supported(Boolean location_supported) {
        this.location_supported = location_supported;
    }

    public String getEarliest_collection_at() {
        return earliest_collection_at;
    }

    public void setEarliest_collection_at(String earliest_collection_at) {
        this.earliest_collection_at = earliest_collection_at;
    }
}
