package apitesting;

import org.slf4j.*;
import org.springframework.http.ResponseEntity;
import org.springframework.util.*;
import org.springframework.web.client.RestTemplate;

import apitesting.util.PropertiesUtils;

/**
 * Created by amarcuta on 19/01/2017.
 */
public class Authentication {

    static Logger logger = LoggerFactory.getLogger(Authentication.class);

    static RestTemplate restTemplate = new RestTemplate();

    public static ResponseEntity<AuthenticationResponse> login(UserAndResult userAndResult) {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("grant_type", "password");
        map.add("username", userAndResult.getUserEmail());
        map.add("password", userAndResult.getPassword());


        String url = PropertiesUtils.STAGE.concat("/oauth/token");
        ResponseEntity<AuthenticationResponse> response = restTemplate.postForEntity(url, map,
                AuthenticationResponse.class);

        AuthenticationResponse authenticationResponse = response.getBody();

        userAndResult.setToken(authenticationResponse.getAccess_token());

        if (logger.isInfoEnabled()) {
            logger.info(userAndResult.toString());
        }

        return response;
    }
}
