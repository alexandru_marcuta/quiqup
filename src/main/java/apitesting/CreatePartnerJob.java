package apitesting;

import apitesting.addresses.Addresses;
import apitesting.util.PropertiesUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * Created by amarcuta on 19/01/2017.
 */
public class CreatePartnerJob {

    static Logger logger = LoggerFactory.getLogger(Addresses.class);

    static RestTemplate restTemplate = new RestTemplate();

    public static ResponseEntity<CreateJobResponse> createJob(UserAndResult userAndResult) {

        String url = PropertiesUtils.STAGE.concat("/partner/jobs");

        HttpHeaders headers = prepareHeaders(userAndResult);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("address1", "11 Sussex Lodge, Sussex Place");
        map.add("address2", "LONDON");

        HttpEntity<?> entity = new HttpEntity<Object>(map, headers);

        ResponseEntity<CreateJobResponse> response = restTemplate.exchange(url, HttpMethod.POST,
                entity, CreateJobResponse.class);

        CreateJobResponse createJobResponse = response.getBody();

        //userAndResult.setAddress1(createJobResponse.getAddress1());
        //userAndResult.setAddress2(createJobResponse.getAddress2());

        if (logger.isInfoEnabled()) {
            logger.info(userAndResult.toString());
        }

        return response;
    }

    private static HttpHeaders prepareHeaders(UserAndResult userAndResult) {

        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer ".concat(userAndResult.getToken()));
        headers.set("Content-Type", "application/json");

        return headers;
    }
}
