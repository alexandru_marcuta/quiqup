package apitesting.addresses.image;

/**
 * Created by amarcuta on 26/01/2017.
 */
public class Medium2x {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
