package apitesting.addresses.image;

/**
 * Created by amarcuta on 26/01/2017.
 */
public class Image {

    private String url;
    private Medium medium;
    private Medium2x medium2x;
    private Small small;
    private Small2x small2x;
    private Square square;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
