package apitesting.addresses;

import java.util.List;

import apitesting.addresses.image.Image;

/**
 * Created by amarcuta on 19/01/2017.
 */
public class AddressesResponse {

    private Integer id;
    private String user_id;
    private Integer job_id;
    private String alt;
    List<Double> coords;
    private String name;
    private String address1;
    private String address2;
    private String town;
    private String county;
    private String postcode;
    private String geo_method;
    private String geo_source;
    private String mid;
    private String created_at;
    private String updated_at;
    private String identifiers;
    private String sync;
    private String notes;
    private String npostcode;
    private String venue_id;
    private Image image;
    private String last_verified;
    private String last_verified_by_id;
    private String operating_times_cache;
    private String phone;
    private Boolean active;
    private String geopoint;
    private String foursquare_id;
    private String fixed_delivery_cost;
    private String google_places_id;
    private Double rating;
    private String meta_updated_at;
    private String contact_name;
    private Boolean google_resolved;
    private String slug;
    private String partner_location_id;
    private String anonymised_atn;
    private Boolean receipt_verification;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public Integer getJob_id() {
        return job_id;
    }

    public void setJob_id(Integer job_id) {
        this.job_id = job_id;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getGeo_method() {
        return geo_method;
    }

    public void setGeo_method(String geo_method) {
        this.geo_method = geo_method;
    }

    public String getGeo_source() {
        return geo_source;
    }

    public void setGeo_source(String geo_source) {
        this.geo_source = geo_source;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getIdentifiers() {
        return identifiers;
    }

    public void setIdentifiers(String identifiers) {
        this.identifiers = identifiers;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNpostcode() {
        return npostcode;
    }

    public void setNpostcode(String npostcode) {
        this.npostcode = npostcode;
    }

    public String getVenue_id() {
        return venue_id;
    }

    public void setVenue_id(String venue_id) {
        this.venue_id = venue_id;
    }

    public String getLast_verified() {
        return last_verified;
    }

    public void setLast_verified(String last_verified) {
        this.last_verified = last_verified;
    }

    public String getLast_verified_by_id() {
        return last_verified_by_id;
    }

    public void setLast_verified_by_id(String last_verified_by_id) {
        this.last_verified_by_id = last_verified_by_id;
    }

    public String getOperating_times_cache() {
        return operating_times_cache;
    }

    public void setOperating_times_cache(String operating_times_cache) {
        this.operating_times_cache = operating_times_cache;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getGeopoint() {
        return geopoint;
    }

    public void setGeopoint(String geopoint) {
        this.geopoint = geopoint;
    }

    public String getFoursquare_id() {
        return foursquare_id;
    }

    public void setFoursquare_id(String foursquare_id) {
        this.foursquare_id = foursquare_id;
    }

    public String getFixed_delivery_cost() {
        return fixed_delivery_cost;
    }

    public void setFixed_delivery_cost(String fixed_delivery_cost) {
        this.fixed_delivery_cost = fixed_delivery_cost;
    }

    public String getGoogle_places_id() {
        return google_places_id;
    }

    public void setGoogle_places_id(String google_places_id) {
        this.google_places_id = google_places_id;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getMeta_updated_at() {
        return meta_updated_at;
    }

    public void setMeta_updated_at(String meta_updated_at) {
        this.meta_updated_at = meta_updated_at;
    }

    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public Boolean getGoogle_resolved() {
        return google_resolved;
    }

    public void setGoogle_resolved(Boolean google_resolved) {
        this.google_resolved = google_resolved;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getPartner_location_id() {
        return partner_location_id;
    }

    public void setPartner_location_id(String partner_location_id) {
        this.partner_location_id = partner_location_id;
    }

    public String getAnonymised_atn() {
        return anonymised_atn;
    }

    public void setAnonymised_atn(String anonymised_atn) {
        this.anonymised_atn = anonymised_atn;
    }

    public Boolean getReceipt_verification() {
        return receipt_verification;
    }

    public void setReceipt_verification(Boolean receipt_verification) {
        this.receipt_verification = receipt_verification;
    }

    public List<Double> getCoords() {
        return coords;
    }

    public void setCoords(List<Double> coords) {
        this.coords = coords;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
