package apitesting.addresses;

import org.slf4j.*;
import org.springframework.http.*;
import org.springframework.util.*;
import org.springframework.web.client.RestTemplate;

import apitesting.*;
import apitesting.util.PropertiesUtils;

/**
 * Created by amarcuta on 19/01/2017.
 */
public class Addresses {

    static Logger logger = LoggerFactory.getLogger(Addresses.class);

    static RestTemplate restTemplate = new RestTemplate();

    public static ResponseEntity<AddressesResponse> addAddress(UserAndResult userAndResult,
                               String address1, String address2, String postcode) {

        HttpHeaders headers = prepareHeaders(userAndResult);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("address1", address1);
        map.add("address2", address2);
        map.add("lookup_id", "2417493");
        map.add("name", null);
        map.add("notes", null);
        map.add("postcode", postcode);

        HttpEntity<?> entity = new HttpEntity<Object>(map, headers);

        String url = PropertiesUtils.STAGE.concat("/customer/addresses");
        ResponseEntity<AddressesResponse> response = restTemplate.exchange(url, HttpMethod.POST,
                entity, AddressesResponse.class);

        AddressesResponse addressesResponse = response.getBody();

        userAndResult.setAddress1(addressesResponse.getAddress1());
        userAndResult.setAddress2(addressesResponse.getAddress2());
        userAndResult.setPostcode(addressesResponse.getPostcode());

        return response;
    }

    private static HttpHeaders prepareHeaders(UserAndResult userAndResult) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer ".concat(userAndResult.getToken()));
        headers.set("Accept", "application/json, text/plain, */*");
        headers.set("Api-Version", "20160101");

        return headers;
    }
}
