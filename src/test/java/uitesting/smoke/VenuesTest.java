package uitesting.smoke;

import net.thucydides.core.annotations.*;

import org.junit.Test;

import java.util.ArrayList;

import uitesting.util.PropertiesUtils;
import uitesting.workflows.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by amarcuta on 24/01/2017.
 */
public class VenuesTest extends AddressTest {

    public static final String THE_QUIQ_BAR_BY_31DOVER = "The Quiq Bar by 31Dover";

    @Steps
    private LogInLogOutWorkflow logInLogOutWorkflow;
    @Steps
    private AddressWorkflow addressWorkflow;
    @Steps
    private VenuesWorkflow venuesWorkflow;

    @Test
    @WithTag(type = "groups", name = "smoke")
    public void testSelectVenue() {
        logInLogOutWorkflow.logIn(PropertiesUtils.getProperty("username"), PropertiesUtils.getProperty("password"));
        addressWorkflow.addPostcode(POSTCODE);
        venuesWorkflow.selectVenue(THE_QUIQ_BAR_BY_31DOVER);
        ArrayList<Boolean> statuses = venuesWorkflow.checkVenue(THE_QUIQ_BAR_BY_31DOVER);
        logInLogOutWorkflow.logOut(webdriver);

        for (Boolean status : statuses) {
            assertEquals(status,Boolean.TRUE);
        }
    }
}
