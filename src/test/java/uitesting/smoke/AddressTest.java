package uitesting.smoke;

import net.thucydides.core.annotations.*;

import org.junit.Test;

import uitesting.util.PropertiesUtils;
import uitesting.workflows.*;

import static com.thoughtworks.selenium.SeleneseTestCase.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by amarcuta on 24/01/2017.
 */
public class AddressTest extends LoginTest {

    public static final String POSTCODE = "W2 2SG";
    public static final String ADDRESS = "7 Sussex Lodge, Sussex Place";

    @Steps
    private LogInLogOutWorkflow logInLogOutWorkflow;
    @Steps
    private AddressWorkflow addressWorkflow;

    @Test
    @WithTag(type = "groups", name = "smoke")
    public void testAddValidPostcodeLoggedIn() {
        logInLogOutWorkflow.logIn(PropertiesUtils.getProperty("username"), PropertiesUtils.getProperty("password"));
        String actualPostcode = addressWorkflow.addPostcode(POSTCODE);
        logInLogOutWorkflow.logOut(webdriver);

        assertTrue(actualPostcode.length() > 0);
        assertEquals(actualPostcode, POSTCODE);
    }

    @Test
    @WithTag(type = "groups", name = "smoke")
    public void testAddValidPostcodeLoggedOff() {
        String actualPostcode = addressWorkflow.addPostcode(POSTCODE);

        assertTrue(actualPostcode.length() > 0);
        assertEquals(actualPostcode, POSTCODE);
    }

}
