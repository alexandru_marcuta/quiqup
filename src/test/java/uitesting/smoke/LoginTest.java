package uitesting.smoke;

import net.thucydides.core.annotations.*;

import org.junit.Test;

import uitesting.BaseTest;
import uitesting.util.PropertiesUtils;
import uitesting.workflows.LogInLogOutWorkflow;

/**
 * Created by amarcuta on 1/17/17.
 */
public class LoginTest extends BaseTest {

    @Steps
    private LogInLogOutWorkflow logInLogOutWorkflow;

    @Test
    @WithTag(type = "groups", name = "smoke")
    public void testLoginLogout() {
        logInLogOutWorkflow.logIn(PropertiesUtils.getProperty("username"), PropertiesUtils.getProperty("password"));
        logInLogOutWorkflow.logOut(webdriver);
    }
}
