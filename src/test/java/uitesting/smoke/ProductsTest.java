package uitesting.smoke;

import net.thucydides.core.annotations.*;

import org.junit.Test;

import uitesting.util.*;
import uitesting.workflows.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by amarcuta on 24/01/2017.
 */
public class ProductsTest extends VenuesTest {

    private static final String GIFT_BOX_WITH_STYLISH_RIBBON = "Gift Box with Stylish Ribbon";
    private static final String CARD_NUMBER = "4242424242424242";
    private static final Integer EXPIRY_MONTH = 10;
    private static final Integer EXPIRY_YEAR = 17;
    private static final Integer CCV = 123;
    private static final String CARD_POSTCODE = "NW106RB";
    private static final String MOBILE_CODE = "0000";

    private static final String CONFIRMATION_MESSAGE = "We have received your order.";


    @Steps
    private LogInLogOutWorkflow logInLogOutWorkflow;
    @Steps
    private AddressWorkflow addressWorkflow;
    @Steps
    private VenuesWorkflow venuesWorkflow;
    @Steps
    private ProductsWorkflow productsWorkflow;
    @Steps
    private CheckoutWorkflow checkoutWorkflow;

    @Test
    @WithTag(type = "groups", name = "smoke")
    public void testSelectProduct() {
        logInLogOutWorkflow.logIn(PropertiesUtils.getProperty("username"), PropertiesUtils.getProperty("password"));
        addressWorkflow.addPostcode(POSTCODE);
        venuesWorkflow.selectVenue(THE_QUIQ_BAR_BY_31DOVER);
        productsWorkflow.selectProduct(GIFT_BOX_WITH_STYLISH_RIBBON);
        Boolean actualResponse = productsWorkflow.checkProduct(GIFT_BOX_WITH_STYLISH_RIBBON);
        logInLogOutWorkflow.logOut(webdriver);

        assertEquals(actualResponse,Boolean.TRUE);
    }

    @Test
    @WithTag(type = "groups", name = "smoke")
    public void testGetProduct() {
        logInLogOutWorkflow.logIn(PropertiesUtils.getProperty("username"), PropertiesUtils.getProperty("password"));
        addressWorkflow.addPostcode(POSTCODE);
        venuesWorkflow.selectVenue(THE_QUIQ_BAR_BY_31DOVER);
        productsWorkflow.selectProduct(GIFT_BOX_WITH_STYLISH_RIBBON);
        productsWorkflow.checkProduct(GIFT_BOX_WITH_STYLISH_RIBBON);
        productsWorkflow.getProductNow();
        logInLogOutWorkflow.logOut(webdriver);
    }

    @Test
    @WithTag(type = "groups", name = "smoke")
    public void testBuyProduct() {
        logInLogOutWorkflow.logIn(PropertiesUtils.getProperty("username"), PropertiesUtils.getProperty("password"));
        addressWorkflow.addPostcode(POSTCODE);
        venuesWorkflow.selectVenue(THE_QUIQ_BAR_BY_31DOVER);
        productsWorkflow.selectProduct(GIFT_BOX_WITH_STYLISH_RIBBON);
        productsWorkflow.checkProduct(GIFT_BOX_WITH_STYLISH_RIBBON);
        productsWorkflow.getProductNow();
        checkoutWorkflow.addNewAddress(POSTCODE, ADDRESS);
        checkoutWorkflow.addNewCard(CARD_NUMBER,EXPIRY_MONTH,EXPIRY_YEAR,CCV,
                CARD_POSTCODE);
        checkoutWorkflow.getProductNow(MOBILE_CODE);
        Boolean actualResponse = checkoutWorkflow.checkConfirmationMessage(CONFIRMATION_MESSAGE);
        String confirmationOrder = checkoutWorkflow.getConfirmationOrder();
        logInLogOutWorkflow.logOut(webdriver);

        String expectedConfirmationMessage = "Order " + DateUtils.getTodayDate();
        assertEquals(actualResponse,Boolean.TRUE);
        assertEquals(StringUtils.getFirstWord(confirmationOrder),expectedConfirmationMessage);

    }
}
