package uitesting;


import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.pages.Pages;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uitesting.requirements.QuiqupVerificationSample;
import uitesting.util.PropertiesUtils;

import static org.junit.Assert.assertTrue;

/**
 * Created by amarcuta on 1/16/17.
 */


@Story(QuiqupVerificationSample.VerifyQuiqupPortal.CheckQuiqupShell.class)
@RunWith(SerenityRunner.class)
public class BaseTest {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @ManagedPages()
    protected Pages pages;

    @Before
    public void init() throws Exception {
        PropertiesUtils.setProperties();
        pages.getConfiguration().setDefaultBaseUrl(PropertiesUtils.getProperty("webdriver.base.url"));
    }

    @After
    public void tearDown() {
        webdriver.manage().deleteAllCookies();
    }

    @Test
    public void testInit() {
        String webDriverUrl = PropertiesUtils.getProperty("webdriver.base.url");
        assertTrue(webDriverUrl.length() > 0);

        if (logger.isInfoEnabled()) {
            logger.info("Webdriver {}", webDriverUrl);
        }
    }
}
