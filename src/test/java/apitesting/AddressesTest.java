package apitesting;

import org.springframework.http.ResponseEntity;
import org.testng.annotations.Test;

import apitesting.addresses.*;

import static org.testng.Assert.*;

/**
 * Created by amarcuta on 19/01/2017.
 */
public class AddressesTest extends AuthenticationTest {

    public static final String ADDRESS_1 = "11 Sussex Lodge, Sussex Place";
    public static final String ADDRESS_2 = "LONDON";
    public static final String POSTCODE = "W2 2SG";

    @Test(dataProvider = "provideUser")
    public void testAddAddress(String userEmail, String password) {
        UserAndResult userAndResult = new UserAndResult(userEmail, password);
        Authentication.login(userAndResult);
        ResponseEntity<AddressesResponse> actualResult = Addresses.addAddress(userAndResult,
                ADDRESS_1, ADDRESS_2, POSTCODE);

        assertTrue(actualResult.getStatusCode().is2xxSuccessful());
        assertNotNull(actualResult.getBody().getAddress1());
        assertNotNull(actualResult.getBody().getAddress2());
        assertNotNull(actualResult.getBody().getPostcode());
        assertEquals(actualResult.getBody().getAddress1(),ADDRESS_1);
        assertEquals(actualResult.getBody().getAddress2(),ADDRESS_2);
        assertEquals(actualResult.getBody().getPostcode(),POSTCODE);
    }
}
