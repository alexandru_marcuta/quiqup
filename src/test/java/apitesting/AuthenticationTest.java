package apitesting;

import org.springframework.http.*;
import org.testng.annotations.*;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;


/**
 * Created by amarcuta on 17/01/2017.
 */
public class AuthenticationTest {

    public static final String EXPIRES_IN = "604800";

    @DataProvider
    public Object[][] provideUser() {
        return new Object[][]{new Object[]
                {"quiqup@mailinator.com","quiquptester"}
        };
    }

    @Test(dataProvider = "provideUser")
    public void testUserLogin(String userEmail, String password) {
        UserAndResult userAndResult = new UserAndResult(userEmail, password);
        ResponseEntity<AuthenticationResponse> actualResult = Authentication.login(userAndResult);

        assertTrue(actualResult.getStatusCode().is2xxSuccessful());
        assertNotNull(actualResult.getBody().getAccess_token());
        assertNotNull(actualResult.getBody().getRefresh_token());
        assertEquals(actualResult.getBody().getExpires_in().toString(), EXPIRES_IN);
    }
}
