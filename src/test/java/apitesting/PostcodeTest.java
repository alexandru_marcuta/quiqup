package apitesting;

import org.springframework.http.ResponseEntity;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

/**
 * Created by amarcuta on 19/01/2017.
 */
public class PostcodeTest  {

    @Test(dataProvider = "validPostcodes", groups="smoke")
    public void testValidPostcode(String postcode) {
        ResponseEntity<PostcodeResponse> actualResult = Postcode.checkPostcode(postcode);
        assertTrue(actualResult.getStatusCode().is2xxSuccessful());
        assertTrue(actualResult.getBody().getLocation_supported());
        assertNotNull(actualResult.getBody().getEarliest_collection_at());
    }

    @Test(dataProvider = "invalidPostcodes", groups="smoke")
    public void testInvalidPostcode(String postcode) {
        ResponseEntity<PostcodeResponse> actualResult = Postcode.checkPostcode(postcode);
        assertTrue(actualResult.getStatusCode().is2xxSuccessful());
        assertFalse(actualResult.getBody().getLocation_supported());
        assertNull(actualResult.getBody().getEarliest_collection_at());
    }

    @DataProvider(parallel = true)
    public Object[][] validPostcodes() {
        return new Object[][]{new Object[]
                {"W2 2SG"}, {"SW5 9NB"}
        };
    }

    @DataProvider(parallel = true)
    public Object[][] invalidPostcodes() {
        return new Object[][]{new Object[]
                {"SL6 8AD"}, {"SL6 8JG"}
        };
    }
}
